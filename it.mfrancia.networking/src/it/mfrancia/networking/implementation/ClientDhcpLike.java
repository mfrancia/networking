package it.mfrancia.networking.implementation;

import it.mfrancia.exceptions.GenericIOException;
import it.mfrancia.interfaces.networking.IClientDhcpLike;
import it.mfrancia.interfaces.networking.IConnection;
import it.mfrancia.interfaces.networking.IConnectionListener;
import it.mfrancia.interfaces.networking.IIp;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class ClientDhcpLike extends Client implements IClientDhcpLike {
	public ClientDhcpLike() {
		super();
	}
	
	@Override
	public IConnection[] discover(final IIp networkIp, final int port) throws GenericIOException {
		final ArrayList<IConnection> c = new ArrayList<IConnection>();
		
		DatagramSocket dgs = null;
		try {
			dgs = new DatagramSocket();
			dgs.setBroadcast(true);
			
			log("running");
			
			final byte[] buf = new byte[NetworkingKb.getPayloadSize()];
			DatagramPacket dgp = new DatagramPacket(buf, NetworkingKb.getPayloadSize());
			
			dgp.setAddress(InetAddress.getByName(networkIp.getIpValue().toString()));
			
			dgp.setPort(port);
			dgp.setData(discoverProtocolStepOne());
			dgs.send(dgp);
			
			dgs.setSoTimeout(NetworkingKb.getDtUdpDiscover());
			final long time = System.currentTimeMillis();
			
			while ((System.currentTimeMillis() - time) < (NetworkingKb.getDtUdpDiscover() * NetworkingKb.getUdpDiscoverRetry())) {
				try {
					dgp = new DatagramPacket(buf, NetworkingKb.getPayloadSize());
					dgs.receive(dgp);
					if (discoverProtocolStepTwo(dgp.getData())) {
						String address = dgp.getAddress().toString();
						if (address.indexOf("/") != -1) {
							address = address.substring(1);
						}
						c.add(new ConnectionSocketChannel(new Socket(address, NetworkingKb.getServerPort())));
					}
				} catch (final SocketTimeoutException e) {
				} catch (final GenericIOException e) {
					e.printStackTrace();
				}
				
			}
		} catch (final IOException e) {
			e.printStackTrace();
		}
		if (dgs != null) {
			dgs.close();
		}
		final IConnection[] ret = new IConnection[c.size()];
		return c != null ? c.toArray(ret) : null;
		
	}
	
	/**
	 * override to re-define the first step of the application protocol relative to the server discover
	 * 
	 * @param receivedData
	 * @return
	 */
	protected byte[] discoverProtocolStepOne() {
		return new byte[] {};
	}
	
	/**
	 * override to re-define the second step of the application protocol relative to the server discover
	 * 
	 * @param receivedData
	 * @return
	 */
	protected boolean discoverProtocolStepTwo(final byte[] receivedData) {
		return true;
	}
	
	@Override
	public IConnectionListener getListener() {
		return super.getListener();
	}
	
	@Override
	public void setConnection(final IConnection con) {
		super.conn = con;
	}
}
