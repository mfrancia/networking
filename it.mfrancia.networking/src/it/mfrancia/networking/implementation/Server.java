package it.mfrancia.networking.implementation;

import it.mfrancia.exceptions.GenericIOException;
import it.mfrancia.exceptions.MyException;
import it.mfrancia.interfaces.networking.IAsyncServerListener;
import it.mfrancia.interfaces.networking.IConnection;
import it.mfrancia.interfaces.networking.IConnectionListener;
import it.mfrancia.interfaces.networking.IPort;
import it.mfrancia.interfaces.networking.IServer;
import it.mfrancia.interfaces.networking.ISocket;
import it.mfrancia.networking.exceptions.ConnectionClosedException;
import it.mfrancia.networking.exceptions.InvalidPortException;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketException;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.Semaphore;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 *
 * @author Matteo Francia
 *
 */
public class Server extends Peer implements IServer {
	
	private HashMap<String, IConnection>	connections;
	private DatagramSocket					dgs;
	private final IPort						discoverablePort;
	private boolean							notStopped;
	private Semaphore						onStart;				// udp service must start after tcp one
	private Selector						selector;
	private Thread							socketChannelHandler;
	private ServerSocketChannel				ss;
	private Thread							udpHandler;
	
	public Server(final CharSequence name, final ISocket socket) {
		this(name, socket, null);
	}
	
	public Server(final CharSequence name, final ISocket socket, final IPort discoverablePort) {
		super(name != null ? "[" + name + NetworkingKb.getUniqueId() + "]" : "[Server" + NetworkingKb.getUniqueId() + "]", socket);
		this.discoverablePort = discoverablePort;
		try {
			config();
		} catch (final IOException e) {
			e.printStackTrace();
			if (listener != null) {
				((IAsyncServerListener) listener).onServerStartedFailed(Server.this, new MyException(e.toString()));
			}
		}
	}
	
	public Server(final ISocket socket) {
		this(null, socket, null);
	}
	
	public Server(final ISocket socket, final IPort discoverablePort) {
		this(null, socket, discoverablePort);
	}
	
	private synchronized void closeConnections() {
		final Iterator<Entry<String, IConnection>> it = connections.entrySet().iterator();
		while (it.hasNext()) {
			final Map.Entry<String, IConnection> pairs = it.next();
			pairs.getValue().close();
			it.remove(); // avoids a ConcurrentModificationException
		}
	}
	
	protected void config() throws IOException {
		notStopped = false;
		
		connections = new HashMap<String, IConnection>();
		
		onStart = new Semaphore(0);
		// create a new serversocketchannel. The channel is unbound.
		ss = ServerSocketChannel.open();
		
		// bind the channel to an address. The channel starts listening to
		// incoming connections.
		
		ss.socket().bind(new InetSocketAddress(getSocket().getIp().getIpValue().toString(), getSocket().getPort().getPortNumber()));
		
		// mark the serversocketchannel as non blocking
		ss.configureBlocking(false);
		
		// create a selector that will by used for multiplexing. The selector
		// registers the socketserverchannel as
		// well as all socketchannels that are created
		selector = Selector.open();
		
		ss.register(selector, SelectionKey.OP_ACCEPT);
		
		socketChannelHandler = new Thread(toString() + "ServerThread") {
			@Override
			public void run() {
				onStart.release();
				log("running server");
				try {
					if (listener != null) {
						new Thread(new Runnable() {
							@Override
							public void run() {
								((IAsyncServerListener) listener).onServerStartedSucceded(Server.this);
							}
						}).start();
					}
					while (notStopped) {
						try {
							final int readyChannels = selector.select();
							if (readyChannels == 0) {
								continue;
							}
							final Set<SelectionKey> selectedKeys = selector.selectedKeys();
							final Iterator<SelectionKey> keyIterator = selectedKeys.iterator();
							while (notStopped && keyIterator.hasNext()) {
								final SelectionKey key = keyIterator.next();
								SocketChannel clientChannel;
								IConnection connection;
								if (key.isAcceptable()) {
									// accept the new connection on the server socket. Since the server socket channel is marked as non blocking this channel will return null if no client is
									// connected.
									if (!notStopped) {
										break;
									}
									clientChannel = ss.accept();
									connection = new ConnectionSocketChannel(clientChannel);
									// devo accettare la connessione?
									if ((listener == null) || !listener.onConnectionAccept(connection)) {
										log("connection refused");
										connection.write("rejected".getBytes());
										connection.close();
									} else {
										// set the client connection to be non blocking
										connection.write("accepted".getBytes());
										clientChannel.configureBlocking(false);
										clientChannel.register(selector, SelectionKey.OP_READ, SelectionKey.OP_WRITE);
										connections.put(connection.getId(), connection);
										log("connection accepted from: " + clientChannel.socket().getRemoteSocketAddress().toString());
										listener.onConnectionConnected(connection);
									}
								} else if (key.isConnectable()) {
									;
								} else if (key.isReadable()) {
									// a channel is ready for reading
									clientChannel = (SocketChannel) key.channel();
									if (notStopped) {
										final IConnection con = connections.get(clientChannel.socket().toString());
										// if the connection is not already closed by server
										if (con != null) {
											try {
												final byte[] b = con.read();
												log("data received from " + clientChannel.socket().getRemoteSocketAddress().toString() + ", data length: " + b.length);
												if (listener != null) {
													listener.onDataReceived(con, b);
												}
											} catch (final ConnectionClosedException e) {
												log("connection closed by client " + con.getPeer().toString());
												if (listener != null) {
													listener.onConnectionClosed(con);
												}
												killConnection(con);
											}
										}
									}
								} else if (key.isWritable()) {
									;
								}
								keyIterator.remove();
							}
						} catch (final CancelledKeyException e) {
							// do not show
						} catch (final GenericIOException e) {
							e.printStackTrace();
						} catch (final InvalidPortException e) {
							e.printStackTrace();
						}
					}
					// log("notStopped = false");
				} catch (final IOException e) {
					e.printStackTrace();
				}
			}
		};
		
		udpHandler = new Thread(toString() + "UDPServer") {
			@Override
			public void run() {
				
				try {
					onStart.acquire();
					dgs = new DatagramSocket(discoverablePort.getPortNumber(), InetAddress.getByName(getSocket().getIp().getIpValue().toString()));
					log("udp discover running on port: " + discoverablePort.getPortNumber());
					if (listener != null) {
						new Thread(new Runnable() {
							@Override
							public void run() {
								((IAsyncServerListener) listener).onDiscoverableServerStartedSucceded(Server.this);
							}
						}).start();
					}
					while (notStopped) {
						try {
							final byte[] buf = new byte[NetworkingKb.getPayloadSize()];
							final DatagramPacket dgp = new DatagramPacket(buf, NetworkingKb.getPayloadSize());
							dgs.receive(dgp);
							if (notStopped && discoverProtocolStepOne(buf)) {
								log("received discover request from " + dgp.getSocketAddress().toString());
								dgp.setSocketAddress(dgp.getSocketAddress());
								dgp.setData(discoverProtocolStepTwo());
								dgs.send(dgp);
							}
						} catch (final SocketException e) { // do not show
						} catch (final IOException e) {
							e.printStackTrace();
						}
					}
				} catch (final Exception e) {
					if (listener != null) {
						((IAsyncServerListener) listener).onDiscoverableServerStartedFailed(Server.this, new MyException(e.toString()));
					}
				}
			}
		};
	}
	
	/**
	 * override to re-define the first step of the application protocol relative to the server discover
	 *
	 * @param receivedData
	 * @return
	 */
	protected boolean discoverProtocolStepOne(final byte[] receivedData) {
		return true;
	}
	
	/**
	 * override to re-define the second step of the application protocol relative to the server discover
	 *
	 * @param receivedData
	 * @return
	 */
	protected byte[] discoverProtocolStepTwo() {
		return new byte[] {};
	}
	
	@Override
	public synchronized IConnection[] getConnections() {
		final IConnection[] conn = new IConnection[connections.values().size()];
		return connections.values().toArray(conn);
	}
	
	@Override
	public synchronized boolean isRunning() {
		return notStopped;
	}
	
	@Override
	public synchronized void kill() {
		if (isRunning()) {
			notStopped = false;
			
			try {
				ss.socket().close();
				ss.close();
				if (dgs != null) {
					dgs.close();
				}
			} catch (final IOException e) {
				e.printStackTrace();
			}
			
			/*
			 * ClosedByInterruptException Channel operations are bound to the thread doing the operations. If this thread is interrupted, the stream / channel is closed due to IO safety issues.
			 */
			socketChannelHandler.interrupt();
			if (udpHandler != null) {
				udpHandler.interrupt();
			}
			
			closeConnections();
			
			log("stopped");
		} else {
			log("Server is already stopped");
		}
	}
	
	@Override
	public synchronized void killConnection(final IConnection c) {
		connections.remove(c.getId());
		c.close();
	}
	
	@Override
	public void setEventListener(final IAsyncServerListener listener) {
		super.setEventListener(listener);
	}
	
	@Deprecated
	@Override
	/**
	 * Use setEventListener(IAsyncServerListener listener) instead
	 */
	public void setEventListener(final IConnectionListener l) {
		throw new NotImplementedException();
	}
	
	@Override
	public synchronized void start() {
		if (!isRunning()) {
			notStopped = true;
			log("server started");
			startServer();
			if (discoverablePort != null) {
				startUdpDiscoverable();
			}
		} else {
			log("Server is already running");
		}
	}
	
	protected void startServer() {
		socketChannelHandler.start();
		
	}
	
	protected void startUdpDiscoverable() {
		udpHandler.start();
	}
	
}
