package it.mfrancia.networking.implementation;

import it.mfrancia.exceptions.GenericIOException;
import it.mfrancia.implementation.logging.Loggable;
import it.mfrancia.interfaces.networking.IConnection;
import it.mfrancia.interfaces.networking.ISocket;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public abstract class AbstractConnection extends Loggable implements IConnection {
	private final ExecutorService	executor;
	private String					id;
	protected ISocket				socket;
	
	public AbstractConnection(final CharSequence name, final ISocket socket) {
		super(name + " " + socket.toString());
		setId(socket.toString());
		this.socket = socket;
		/*
		 * Creates an Executor that uses a single worker thread operating off an unbounded queue. (Note however that if this single thread terminates due to a failure during execution prior to
		 * shutdown, a new one will take its place if needed to execute subsequent tasks.) Tasks are guaranteed to execute sequentially, and no more than one task will be active at any given time.
		 * Unlike the otherwise equivalent newFixedThreadPool(1) the returned executor is guaranteed not to be reconfigurable to use additional threads.
		 */
		executor = Executors.newSingleThreadExecutor();
	}
	
	@Override
	public void addOutput(final byte[] o) {
		executor.execute(new Runnable() {
			@Override
			public void run() {
				try {
					write(o);
				} catch (final GenericIOException e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	@Override
	public void close() {
		executor.shutdownNow();
	}
	
	@Override
	public String getId() {
		return id;
	}
	
	@Override
	public ISocket getPeer() {
		return socket;
	}
	
	protected void setId(final String id) {
		this.id = id;
	}
}
