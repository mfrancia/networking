package it.mfrancia.networking.implementation;

import it.mfrancia.exceptions.GenericIOException;
import it.mfrancia.interfaces.networking.ISocket;
import it.mfrancia.networking.exceptions.ConnectionClosedException;
import it.mfrancia.networking.exceptions.ErrorOnReadException;
import it.mfrancia.networking.exceptions.ErrorOnWriteException;
import it.mfrancia.networking.exceptions.HostNotConnectedException;
import it.mfrancia.networking.exceptions.InvalidPortException;
import it.mfrancia.networking.exceptions.MessageExceedPayloadSizeException;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.SocketChannel;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class ConnectionSocketChannel extends AbstractConnection {
	private SocketChannel	socketChannel;
	
	public ConnectionSocketChannel(final ISocket destination) throws GenericIOException {
		super("[SCConn" + NetworkingKb.getUniqueId() + "]", destination);
	}
	
	public ConnectionSocketChannel(final SocketChannel socketChannel) throws InvalidPortException {
		super("[SCConn" + NetworkingKb.getUniqueId() + "]", new Socket(socketChannel.socket().getInetAddress().toString(), new Port(socketChannel.socket().getPort())));
		this.socketChannel = socketChannel;
		setId(socketChannel.socket().toString());
	}
	
	@Override
	public synchronized void close() {
		super.close();
		try {
			if (socketChannel != null) {
				socketChannel.close();
			}
		} catch (final IOException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public synchronized void connect() throws GenericIOException {
		try {
			socketChannel = SocketChannel.open();
			if (!socketChannel.isConnected()) {
				socketChannel.connect(new InetSocketAddress(socket.getIp().getIpValue().toString(), socket.getPort().getPortNumber()));
				final String r = new String(readData()).trim();
				if (r.toString().equals("accepted")) {
					log("connected");
				} else {
					log("rejected");
					close();
				}
			}
		} catch (final ClosedChannelException e) {
			
		} catch (final IOException e) {
			throw new GenericIOException(e.toString());
		}
	}
	
	@Override
	public ISocket getPeer() {
		return socket;
	}
	
	@Override
	public synchronized boolean isConnected() {
		return socketChannel != null ? socketChannel.isConnected() : false;
	}
	
	/**
	 * First a Buffer is allocated. The data read from the SocketChannel is read into the Buffer. Second the SocketChannel.read() method is called. This method reads data from the SocketChannel into
	 * the Buffer. The int returned by the read() method tells how many bytes were witten into the Buffer. If -1 is returned, the end-of-stream is reached (the connection is closed).
	 * 
	 * @throws HostNotConnectedException
	 * @throws ErrorOnReadException
	 */
	@Override
	public byte[] read() throws HostNotConnectedException, ErrorOnReadException, ConnectionClosedException {
		if (!isConnected()) {
			throw new HostNotConnectedException("");
		}
		
		return readData();
	}
	
	private byte[] readData() throws ErrorOnReadException, ConnectionClosedException {
		ByteBuffer buf = ByteBuffer.allocate(NetworkingKb.tcpMessageLength);
		try {
			if (socketChannel.read(buf) < NetworkingKb.tcpMessageLength) {
				close();
			}
			
			final byte[] lengthByte = buf.array();
			
			int length = 0;
			length += lengthByte[0] & 0x000000FF;
			length += (lengthByte[1] << 8) & 0x0000FF00;
			length += (lengthByte[2] << 16) & 0x00FF0000;
			length += (lengthByte[3] << 32) & 0xFF000000;
			
			log("Read Length is " + length);
			
			if (length < 0) {
				throw new ErrorOnReadException("Invalid message length " + length);
			}
			buf = ByteBuffer.allocate(length);
			if (socketChannel.read(buf) < length) {
				close();
			}
			
		} catch (final ClosedChannelException e) {
			throw new ConnectionClosedException(e.toString());
		} catch (final IOException e) {
			throw new ErrorOnReadException(e.toString());
		}
		return buf.array();
	}
	
	/**
	 * Writing data to a SocketChannel is done using the SocketChannel.write() method, which takes a Buffer as parameter. Notice how the SocketChannel.write() method is called inside a while-loop.
	 * There is no guarantee of how many bytes the write() method writes to the SocketChannel. Therefore we repeat the write() call until the Buffer has no further bytes to write.
	 * 
	 * @throws HostNotConnectedException
	 * @throws ErrorOnWriteException
	 */
	@Override
	public void write(final byte[] data) throws GenericIOException {
		
		if (!isConnected()) {
			throw new HostNotConnectedException("");
		}
		
		if (data.length > NetworkingKb.getPayloadSize()) {
			throw new MessageExceedPayloadSizeException(data.length + " > " + NetworkingKb.getPayloadSize());
		}
		
		final ByteBuffer buf = ByteBuffer.allocate(NetworkingKb.tcpMessageLength + data.length);
		buf.clear();
		
		// buf.putInt(data.length); //message length
		final int length = data.length;
		log("Write Length is " + length);
		final byte[] lengthByte = new byte[NetworkingKb.tcpMessageLength];
		
		lengthByte[0] = (byte) (length & 0xFF);
		lengthByte[1] = (byte) ((length >> 8) & 0xFF);
		lengthByte[2] = (byte) ((length >> 16) & 0xFF);
		lengthByte[3] = (byte) ((length >> 24) & 0xFF);
		
		buf.put(lengthByte);
		buf.put(data);
		
		buf.flip();
		try {
			while (buf.hasRemaining()) {
				socketChannel.write(buf);
			}
		} catch (final IOException e) {
			close();
			throw new ErrorOnWriteException(e.toString());
		}
	}
	
}
