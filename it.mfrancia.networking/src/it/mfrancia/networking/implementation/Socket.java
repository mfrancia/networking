package it.mfrancia.networking.implementation;

import it.mfrancia.interfaces.networking.IIp;
import it.mfrancia.interfaces.networking.IPort;
import it.mfrancia.interfaces.networking.ISocket;

/**
 * Generic Socket
 * 
 * @author Matteo Francia
 * 
 */
public class Socket implements ISocket {
	private final IIp	ip;
	private final IPort	port;
	
	/**
	 * Create new socket 0.0.0.0:0
	 */
	public Socket() {
		ip = new Ip();
		port = new Port();
	}
	
	/**
	 * Create new socket
	 * 
	 * @param addr
	 *            Ip address
	 * @param port
	 *            Port number
	 */
	public Socket(final CharSequence addr, final IPort port) {
		ip = new Ip(addr);
		this.port = port;
	}
	
	@Override
	/**
	 * Return the ip address
	 */
	public IIp getIp() {
		return ip;
	}
	
	/**
	 * Return the port
	 */
	@Override
	public IPort getPort() {
		return port;
	}
	
	@Override
	public String toString() {
		return ip.getIpValue() + ":" + port.getPortNumber();
	}
}
