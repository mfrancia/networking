package it.mfrancia.networking.implementation;

import it.mfrancia.interfaces.networking.IPort;
import it.mfrancia.networking.exceptions.InvalidPortException;

/**
 * 
 * @author Matteo Francia
 * 
 */
public class Port implements IPort {
	private final int	p;
	
	public Port() {
		p = IPort.ANY;
	}
	
	public Port(final int port) throws InvalidPortException {
		if ((port > IPort.MAX) || (port < IPort.ANY)) {
			throw new InvalidPortException(port + " is an invalid port");
		}
		p = port;
	}
	
	@Override
	public int getPortNumber() {
		return p;
	}
	
}
