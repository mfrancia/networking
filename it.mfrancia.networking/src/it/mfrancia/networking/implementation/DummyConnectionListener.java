package it.mfrancia.networking.implementation;

import it.mfrancia.interfaces.networking.IConnection;
import it.mfrancia.interfaces.networking.IConnectionListener;

/**
 * Dummy event listener, it does nothing
 * 
 * @author Matteo Francia
 * 
 */
public class DummyConnectionListener implements IConnectionListener {
	
	@Override
	public boolean onConnectionAccept(final IConnection con) {
		return true;
	}
	
	@Override
	public void onConnectionClosed(final IConnection con) {
		
	}
	
	@Override
	public void onConnectionConnected(final IConnection con) {
		
	}
	
	@Override
	public void onDataReceived(final IConnection fromConn, final byte[] data) {
		
	}
	
}
