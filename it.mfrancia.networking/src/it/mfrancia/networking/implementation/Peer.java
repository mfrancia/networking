package it.mfrancia.networking.implementation;

import it.mfrancia.implementation.logging.Loggable;
import it.mfrancia.interfaces.networking.IConnectionListener;
import it.mfrancia.interfaces.networking.IPeer;
import it.mfrancia.interfaces.networking.ISocket;

/**
 *
 * @author Matteo Francia
 *
 */
public abstract class Peer extends Loggable implements IPeer {
	protected IConnectionListener	listener;
	private final ISocket			socket;
	
	public Peer(final CharSequence name) {
		super(name);
		socket = new Socket();
	}
	
	public Peer(final CharSequence name, final ISocket socket) {
		super(name);
		this.socket = socket;
	}
	
	@Override
	public ISocket getSocket() {
		return socket;
	}
	
	@Override
	public void setEventListener(final IConnectionListener l) {
		listener = l;
	}
	
}
