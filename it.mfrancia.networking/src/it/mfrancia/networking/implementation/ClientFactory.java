package it.mfrancia.networking.implementation;

import it.mfrancia.exceptions.GenericIOException;
import it.mfrancia.interfaces.networking.IAsyncClient;
import it.mfrancia.interfaces.networking.IAsyncClientListener;
import it.mfrancia.interfaces.networking.IClient;
import it.mfrancia.interfaces.networking.IClientFactory;
import it.mfrancia.interfaces.networking.IConnectionFactory.Type;
import it.mfrancia.interfaces.networking.IConnectionListener;
import it.mfrancia.interfaces.networking.IIp;
import it.mfrancia.interfaces.networking.ISocket;
import it.mfrancia.networking.async.implementation.AsyncClient;

public class ClientFactory implements IClientFactory {
	
	@Override
	public IAsyncClient getAsyncClient(final CharSequence name, final Type type, final ISocket socket, final IAsyncClientListener listener) {
		final IAsyncClient c = new AsyncClient(name, listener, new ConnectionFactory().createConnection(type, socket));
		c.setEventListener(listener);
		try {
			c.connect();
		} catch (final GenericIOException e) {
		}
		return c;
	}
	
	public IAsyncClient getAsyncClient(final Type type, final ISocket socket, final IAsyncClientListener listener) {
		return getAsyncClient(null, type, socket, listener);
	}
	
	@Override
	public IClient getClient(final CharSequence name, final Type type, final ISocket socket, final IConnectionListener listener) {
		final IClient c = new Client(name, new ConnectionFactory().createConnection(type, socket));
		c.setEventListener(listener);
		try {
			c.connect();
		} catch (final GenericIOException e) {
		}
		c.startActiveReading();
		return c;
	}
	
	public IClient getClient(final Type type, final ISocket socket, final IConnectionListener listener) {
		return getClient(null, type, socket, listener);
	}
	
	@Override
	public IAsyncClient getLocalhostAsyncClient(final CharSequence name, final IAsyncClientListener listener) {
		return getAsyncClient(name, Type.SOCKETCHANNEL, new Socket(IIp.LOCALHOST, NetworkingKb.getServerPort()), listener);
	}
	
	public IAsyncClient getLocalhostAsyncClient(final IAsyncClientListener listener) {
		return getAsyncClient(null, Type.SOCKETCHANNEL, new Socket(IIp.LOCALHOST, NetworkingKb.getServerPort()), listener);
	}
	
	@Override
	public IClient getLocalhostClient(final CharSequence name, final IConnectionListener listener) {
		return getClient(name, Type.SOCKETCHANNEL, new Socket(IIp.LOCALHOST, NetworkingKb.getServerPort()), listener);
	}
	
	public IClient getLocalhostClient(final IConnectionListener listener) {
		return getClient(null, Type.SOCKETCHANNEL, new Socket(IIp.LOCALHOST, NetworkingKb.getServerPort()), listener);
	}
	
}
