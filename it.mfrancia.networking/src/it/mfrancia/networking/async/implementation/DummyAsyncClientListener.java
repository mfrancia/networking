package it.mfrancia.networking.async.implementation;

import it.mfrancia.exceptions.MyException;
import it.mfrancia.interfaces.networking.IAsyncClientListener;
import it.mfrancia.interfaces.networking.IClient;
import it.mfrancia.interfaces.networking.IConnection;

/**
 * It does nothing
 * 
 * @author Matteo Francia
 * 
 */
public class DummyAsyncClientListener implements IAsyncClientListener {
	
	@Override
	public void onClientConnected(final IClient client) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public boolean onConnectionAccept(final IConnection con) {
		// TODO Auto-generated method stub
		return false;
	}
	
	@Override
	public void onConnectionClosed(final IConnection con) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onConnectionConnected(final IConnection con) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onConnectionFailed(final MyException e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onDataReceived(final IConnection fromConn, final byte[] data) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onDiscoverFailed(final MyException e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onDiscoverTerminated(final IConnection[] conn) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onWriteFailed(final MyException e) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void onWriteSucceded() {
		// TODO Auto-generated method stub
		
	}
	
}
