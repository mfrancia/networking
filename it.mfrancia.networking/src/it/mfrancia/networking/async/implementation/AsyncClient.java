package it.mfrancia.networking.async.implementation;

import it.mfrancia.exceptions.GenericIOException;
import it.mfrancia.implementation.logging.Loggable;
import it.mfrancia.interfaces.logging.ILogger;
import it.mfrancia.interfaces.networking.IAsyncClient;
import it.mfrancia.interfaces.networking.IAsyncClientListener;
import it.mfrancia.interfaces.networking.IConnection;
import it.mfrancia.interfaces.networking.IConnectionListener;
import it.mfrancia.interfaces.networking.IIp;
import it.mfrancia.interfaces.networking.ISocket;
import it.mfrancia.networking.implementation.ClientDhcpLike;
import it.mfrancia.networking.implementation.NetworkingKb;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import sun.reflect.generics.reflectiveObjects.NotImplementedException;

/**
 * Generic Asynchronous Client, it allows you to interact with the network in an asynchronous way. In order to so an {@link IAsyncClientListener} must be provided.
 * 
 * @author Matteo Francia
 * 
 */
public class AsyncClient extends Loggable implements IAsyncClient {
	private final ClientDhcpLike	client;
	private final ExecutorService	executor;
	
	/**
	 * Create an {@link AsyncClient} without specifying any connection
	 * 
	 * @param listener
	 *            {@link IAsyncClientListener} listener
	 */
	public AsyncClient(final CharSequence name, final IAsyncClientListener listener) {
		this(name, listener, null);
	}
	
	/**
	 * Create an {@link AsyncClient} with the specified connection and name
	 * 
	 * @param listener
	 *            {@link IAsyncClientListener} listener
	 * @param conn
	 *            Connection
	 */
	public AsyncClient(final CharSequence name, final IAsyncClientListener listener, final IConnection conn) {
		super(name == null ? "[AsyncClient" + NetworkingKb.getUniqueId() + "]" : "[" + name + NetworkingKb.getUniqueId() + "]");
		executor = Executors.newSingleThreadExecutor();
		client = new ClientDhcpLike();
		client.setConnection(conn);
		client.setEventListener(listener);
	}
	
	/**
	 * Create an {@link AsyncClient} without specifying any connection
	 * 
	 * @param listener
	 *            {@link IAsyncClientListener} listener
	 */
	public AsyncClient(final IAsyncClientListener listener) {
		this(null, listener, null);
	}
	
	/**
	 * Create an {@link AsyncClient} with the specified connection
	 * 
	 * @param listener
	 *            {@link IAsyncClientListener} listener
	 * @param conn
	 *            Connection
	 */
	public AsyncClient(final IAsyncClientListener listener, final IConnection conn) {
		this(null, listener, conn);
	}
	
	@Override
	public void addOutput(final byte[] o) {
		write(o);
	}
	
	/**
	 * Connect to the specified peer in an asynchronous way. When the client is connected {@link IAsyncClientListener}.OnClientConnected is invoked else it is executed the method
	 * {@link IAsyncClientListener} .onConnectionFailed. If everything goes well the client starts to read the incoming data autonomously, when something appens OnDataReceived is invoked.
	 */
	@Override
	public void connect() {
		if (!client.isConnected()) {
			executor.execute(new Runnable() {
				@Override
				public void run() {
					try {
						client.connect();
						if (client.isConnected()) {
							if (getListener() != null) {
								startActiveReading();
								getListener().onClientConnected(AsyncClient.this);
							}
						}
					} catch (final GenericIOException e) {
						if (getListener() != null) {
							getListener().onConnectionFailed(e);
						}
					}
				}
			});
		}
	}
	
	@Override
	public IConnection[] discover(final IIp networkIp, final int port) {
		executor.execute(new Runnable() {
			@Override
			public void run() {
				try {
					getListener().onDiscoverTerminated(new ClientDhcpLike().discover(networkIp, port));
				} catch (final GenericIOException e) {
					getListener().onDiscoverFailed(e);
				}
			}
		});
		return null;
	}
	
	protected IAsyncClientListener getListener() {
		return (IAsyncClientListener) client.getListener();
	}
	
	@Override
	public ISocket getSocket() {
		return client.getSocket();
	}
	
	@Override
	public boolean isConnected() {
		return client.isConnected();
	}
	
	@Override
	public void kill() {
		client.kill();
		executor.shutdownNow();
	}
	
	@Deprecated
	@Override
	/**
	 * Do not use this method
	 */
	public byte[] read() {
		throw new NotImplementedException();
	}
	
	@Override
	public void setConnection(final IConnection conn) {
		client.setConnection(conn);
	}
	
	@Override
	public void setEventListener(final IAsyncClientListener listener) {
		client.setEventListener(listener);
	}
	
	@Deprecated
	@Override
	/**
	 * Use setEventListener(IAsyncClientEventListener l) instead
	 */
	public void setEventListener(final IConnectionListener l) {
		throw new NotImplementedException();
	}
	
	@Override
	public void setLogger(final ILogger logger) {
		client.setLogger(logger);
	}
	
	@Override
	public void startActiveReading() {
		client.startActiveReading();
	}
	
	/**
	 * Write the specified data in an asynchronous way. When the data are sent {@link IAsyncClientListener}.onWriteSucceded is invoked else it is executed the method {@link IAsyncClientListener}
	 * .onWriteFailed
	 */
	@Override
	public void write(final byte[] data) {
		executor.execute(new Runnable() {
			@Override
			public void run() {
				try {
					client.write(data);
					if (getListener() != null) {
						getListener().onWriteSucceded();
					}
				} catch (final GenericIOException e) {
					if (getListener() != null) {
						getListener().onWriteFailed(e);
					}
				}
			}
		});
	}
	
}
