package it.mfrancia.networking.exceptions;

import it.mfrancia.exceptions.GenericIOException;

public class ErrorOnWriteException extends GenericIOException {
	private static final long	serialVersionUID	= 1L;
	
	public ErrorOnWriteException(final String c) {
		super(c);
	}
	
}
