package it.mfrancia.networking.exceptions;

import it.mfrancia.exceptions.GenericIOException;

public class HostNotConnectedException extends GenericIOException {
	private static final long	serialVersionUID	= 1L;
	
	public HostNotConnectedException(final String c) {
		super(c);
	}
}
