package it.mfrancia.networking.exceptions;

import it.mfrancia.exceptions.GenericIOException;

public class MessageExceedPayloadSizeException extends GenericIOException {
	private static final long	serialVersionUID	= 1L;
	
	public MessageExceedPayloadSizeException(final String c) {
		super(c);
	}
	
}
