package it.mfrancia.networking.main.example1;

import it.mfrancia.exceptions.GenericIOException;
import it.mfrancia.interfaces.logging.ILogger;
import it.mfrancia.interfaces.networking.IConnection;
import it.mfrancia.interfaces.networking.IIp;
import it.mfrancia.interfaces.networking.ISocket;
import it.mfrancia.networking.async.implementation.DummyAsyncServerListener;
import it.mfrancia.networking.exceptions.InvalidPortException;
import it.mfrancia.networking.implementation.NetworkingKb;
import it.mfrancia.networking.implementation.Server;
import it.mfrancia.networking.implementation.Socket;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ServerMain {
	public static void main(final String[] args) throws Exception, InvalidPortException {
		final ServerMain s = new ServerMain(new Socket(IIp.ANY, NetworkingKb.getServerPort()), null);
		
		final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		while (!br.readLine().equals("")) {
			;
		}
		
		s.kill();
	}
	
	private final ILogger	out;
	private Server			server;
	
	private final ISocket	socket;
	
	public ServerMain(final ISocket socket, final ILogger out) throws Exception {
		this.socket = socket;
		this.out = out;
		config();
		start();
	}
	
	private void config() throws Exception {
		// discoverable server on port Syskb.udpDiscoverPort
		server = new Server(socket, NetworkingKb.getUdpDiscoverPort());
		if (out != null) {
			server.setLogger(out);
		}
		server.setEventListener(new DummyAsyncServerListener() {
			
			@Override
			public void onDataReceived(final IConnection from, final byte[] data) {
				// System.out.println(new String(data));
				try {
					from.write(data);
				} catch (final GenericIOException e) {
					e.printStackTrace();
				}
			}
		});
		
	}
	
	public void kill() {
		server.kill();
	}
	
	private void start() {
		// server already started
	}
}
