package it.mfrancia.networking.main.remotelogging;

import it.mfrancia.exceptions.MyException;
import it.mfrancia.interfaces.IOutput;
import it.mfrancia.interfaces.logging.ILogger;
import it.mfrancia.interfaces.networking.IAsyncServerListener;
import it.mfrancia.interfaces.networking.IConnection;
import it.mfrancia.interfaces.networking.IIp;
import it.mfrancia.interfaces.networking.IServer;
import it.mfrancia.networking.implementation.NetworkingKb;
import it.mfrancia.networking.implementation.Server;
import it.mfrancia.networking.implementation.Socket;

public class LoggerSever implements IAsyncServerListener {
	private final IOutput<String>	localLogger;
	private final Server			server;
	
	public LoggerSever(final ILogger localLogger) {
		server = new Server("LoggerServer", new Socket(IIp.ANY, NetworkingKb.getLoggerPort()), NetworkingKb.getDiscoverableLoggerPort());
		server.setLogger(localLogger);
		server.setEventListener(this);
		server.start();
		this.localLogger = localLogger;
	}
	
	public synchronized void kill() {
		server.kill();
	}
	
	@Override
	public boolean onConnectionAccept(final IConnection con) {
		return true;
	}
	
	@Override
	public synchronized void onConnectionClosed(final IConnection con) {
		if (server.getConnections().length == 0) {
			server.kill();
		}
	}
	
	@Override
	public void onConnectionConnected(final IConnection con) {
	}
	
	@Override
	public synchronized void onDataReceived(final IConnection fromConn, final byte[] data) {
		final String s = new String(data);
		if (localLogger != null) {
			localLogger.addOutput(s);
		} else {
			System.out.println(s);
		}
	}
	
	@Override
	public void onDiscoverableServerStartedFailed(final IServer server, final MyException ex) {
	}
	
	@Override
	public void onDiscoverableServerStartedSucceded(final IServer server) {
	}
	
	@Override
	public void onServerStartedFailed(final IServer server, final MyException ex) {
	}
	
	@Override
	public void onServerStartedSucceded(final IServer server) {
	}
}
