package it.mfrancia.networking.main.remotelogging;

import it.mfrancia.exceptions.GenericIOException;
import it.mfrancia.exceptions.MyException;
import it.mfrancia.interfaces.IOutput;
import it.mfrancia.interfaces.logging.ILog;
import it.mfrancia.interfaces.logging.ILog.Priority;
import it.mfrancia.interfaces.logging.ILogger;
import it.mfrancia.interfaces.networking.IAsyncClientListener;
import it.mfrancia.interfaces.networking.IClient;
import it.mfrancia.interfaces.networking.IConnection;
import it.mfrancia.interfaces.networking.IIp;
import it.mfrancia.networking.async.implementation.AsyncClient;
import it.mfrancia.networking.implementation.Ip;
import it.mfrancia.networking.implementation.NetworkingKb;

/**
 * Remote logger
 * 
 * @author Matteo Francia
 *
 */
public class LoggerClient implements ILogger, IAsyncClientListener {
	private final AsyncClient		client;
	private AsyncClient[]			clients;
	private final IOutput<String>	localLogger;
	private int						retry	= 3;
	
	public LoggerClient() throws GenericIOException {
		this(null);
	}
	
	/**
	 * It connects to all the existing logger servers
	 * 
	 * @param localLogger
	 *            local logger
	 * @throws GenericIOException
	 */
	public LoggerClient(final ILogger localLogger) throws GenericIOException {
		this.localLogger = localLogger;
		client = new AsyncClient("LoggerClient", this);
		client.setLogger(localLogger);
		client.discover(new Ip(IIp.ANY), NetworkingKb.getDiscoverableLoggerPort().getPortNumber());
	}
	
	/**
	 * Log the specified string
	 */
	@Override
	public synchronized void addOutput(final String o) {
		if (clients != null) {
			for (final AsyncClient c : clients) {
				c.write(o.getBytes());
			}
		}
		if (localLogger != null) {
			localLogger.addOutput(o);
		}
	}
	
	public synchronized void kill() {
		client.kill();
		if (clients != null) {
			for (final AsyncClient c : clients) {
				c.kill();
			}
		}
	}
	
	@Override
	public void log(final CharSequence message) {
		addOutput(message.toString());
	}
	
	@Override
	public void log(final ILog log) {
		log(log.getPriority(), log.getMessage());
	}
	
	@Override
	public void log(final Priority priority, final CharSequence message) {
		log(priority.toString() + " " + message.toString());
	}
	
	@Override
	public void onClientConnected(final IClient client) {
	}
	
	@Override
	public boolean onConnectionAccept(final IConnection con) {
		return false;
	}
	
	@Override
	public void onConnectionClosed(final IConnection con) {
	}
	
	@Override
	public void onConnectionConnected(final IConnection con) {
	}
	
	@Override
	public void onConnectionFailed(final MyException e) {
	}
	
	@Override
	public void onDataReceived(final IConnection fromConn, final byte[] data) {
	}
	
	@Override
	public synchronized void onDiscoverFailed(final MyException e) {
		if (retry-- >= 0) {
			client.discover(new Ip(IIp.ANY), NetworkingKb.getDiscoverableLoggerPort().getPortNumber());
		}
	}
	
	@Override
	public void onDiscoverTerminated(final IConnection[] conn) {
		if (conn != null) {
			clients = new AsyncClient[conn.length];
			for (int i = 0; i < conn.length; i++) {
				clients[i] = new AsyncClient(this, conn[i]);
			}
		}
	}
	
	@Override
	public void onWriteFailed(final MyException e) {
	}
	
	@Override
	public void onWriteSucceded() {
	}
	
}
