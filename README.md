# README #

Realize a little client server infrastructure using any kind of sockets.
The server must be implemented in order to avoid having a thread for
every connection. Clients and Server have to be on the same network
(LAN) and it is required a mechanism to discover which peer is playing
the server role. It is possible to have more than one server.