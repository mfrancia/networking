package it.mfrancia.networking.android;

import it.mfrancia.implementation.logging.Logger;
import it.mfrancia.interfaces.IOutput;
import it.mfrancia.interfaces.logging.ILogger;
import it.mfrancia.interfaces.networking.IServer;
import it.mfrancia.networking.async.implementation.AsyncClient;
import it.mfrancia.networking.async.implementation.DummyAsyncClientListener;
import it.mfrancia.networking.async.implementation.DummyAsyncServerListener;
import it.mfrancia.networking.implementation.ClientFactory;
import it.mfrancia.networking.implementation.NetworkingKb;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity implements IOutput<String> {
	private AsyncClient	client;
	private ILogger		localLogger;
	private ILogger		logger;
	private TextView	text;
	
	@Override
	public void addOutput(final String o) {
		print(o);
	}
	
	public void onClick(final View v) {
		
		switch (v.getId()) {
			case R.id.button1: {
				onCreateEchoServerClick(v);
				break;
			}
			
			case R.id.button2: {
				onCreateEchoClientClick(v);
				break;
			}
			
			case R.id.button3: {
				onCreateLoggerServerClick(v);
				break;
			}
			case R.id.button4: {
				onCreateLoggerClientClick(v);
				break;
			}
		}
	}
	
	@Override
	protected void onCreate(final Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_menu);
		text = (TextView) findViewById(R.id.textView1);
		localLogger = new Logger(this);
	}
	
	public void onCreateEchoClientClick(final View v) {
		if (client != null) {
			client.kill();
		}
		client = (AsyncClient) new ClientFactory().getLocalhostAsyncClient("EchoClient", new DummyAsyncClientListener());
		client.setLogger(localLogger);
		for (int i = 0; i < 10; i++) {
			client.write("hello, world!".getBytes());
		}
	}
	
	public void onCreateEchoServerClick(final View v) {
		NetworkingKb.destroyServerInstance();
		final IServer server = NetworkingKb.getDiscoverableServerInstance(new DummyAsyncServerListener() {
			@Override
			public void onDataReceived(final it.mfrancia.interfaces.networking.IConnection fromConn, final byte[] data) {
				fromConn.addOutput(data);
				print("echo: " + new String(data).toString());
			};
		});
		server.setLogger(localLogger);
	}
	
	public void onCreateLoggerClientClick(final View v) {
		NetworkingKb.destroyLoggerClient();
		try {
			logger = NetworkingKb.getLoggerClient(localLogger);
			for (int i = 0; i < 10; i++) {
				logger.log("hello, world!");
			}
		} catch (final Exception e) {
			print(e.toString());
		}
	}
	
	public void onCreateLoggerServerClick(final View v) {
		NetworkingKb.destroyLoggerServer();
		NetworkingKb.getLoggerServer(localLogger);
	}
	
	private void print(final String s) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				text.append(s + "\n");
			}
		});
	}
	
}
